import { ArrayExtensions, NeutralSpawnerType, EventsSDK, EntityManager,GameSleeper , Hero, LocalPlayer, Menu, Unit, Vector3, Vector2,item_blink,item_phase_boots,bounty_hunter_wind_walk } from "wrapper/Imports";
import { EntityX, GameX } from "X-Core/Imports";

const Test = Menu.AddEntryDeep(["Utility", "Afk Farm"])
const Toggle = Test.AddToggle("Включить")

EventsSDK.on("Tick", () => {
	if (Toggle.value && GameX.IsInGame)
	{
		const Sleeper = new GameSleeper()
		const localHero = LocalPlayer.Hero
		const ListOfTargets = new Map<Unit, [Unit, boolean]>()

		const array_map = [...EntityX.Camps.values()]
		let camp = ArrayExtensions.orderBy(array_map.filter(x => (x.Owners.includes(localHero))
			&& !x.IsEmpty
			&& (localHero.FarmEnemyCamp || x.IsAlly)
			&& (localHero.FarmAncients || x.CampType !== NeutralSpawnerType.Ancient),
		), x => x.Position.Distance2D(localHero.Position))[0]

		if (camp === undefined)
			camp = ArrayExtensions.orderBy(array_map.filter(x => !x.IsEmpty
				&& (localHero.FarmAncients || x.CampType !== NeutralSpawnerType.Ancient)
				&& (localHero.FarmEnemyCamp || x.IsAlly)), x => x.Position.Distance2D(localHero.Position))[0]

		if (camp !== undefined) {
			if (!camp.Owners.some(x => x.Index === localHero.Index))
				camp.Owners.push(localHero)

			const target = EntityX.EnemyCreeps.find(unit => unit.IsVisible
				&& !unit.IsLaneCreep
				&& unit.IsSpawned
				&& unit.IsAlive
				&& localHero.Distance2D(unit) <= 500
			)

			//console.log(target);

			if (target === undefined && localHero.Distance2D(camp.Position) <= 100) {
				camp.IsEmpty = true
				return
			}

			if (target !== undefined) {
				AttackTarget(target)
				return
			}

			//EntityManager.GetEntitiesByClass(item_phase_boots).forEach(abil => {
			//	const abill = abil;
			//	if (abil === undefined)
			//		return

			//	if ((!abil.CanBeCasted() || Sleeper.Sleeping(abil.Index)))
			//		return

			//	abil.UseAbility();
			//	Sleeper.Sleep((abil.CastPoint * 1000 + 50), abil.Index)
			//})
			MoveToPosition(camp.Position)
			//console.log(camp.Position)
		}

		function AttackTarget(target: Unit) {
			const last_owner = ListOfTargets.get(localHero)
			if (last_owner === undefined) {
				localHero.AttackTarget(target)
				ListOfTargets.set(localHero, [target, true])
				return
			}
			if (!last_owner[0].IsAlive || !last_owner[0].IsVisible) {
				ListOfTargets.delete(localHero)
				return
			}
			if (last_owner[1])
				return
			localHero.AttackTarget(target)
		}

		function MoveToPosition(vector: Vector3) {
			if (localHero.IsMoving)
				return

			localHero.AttackMove(vector)
		}
	}
})

